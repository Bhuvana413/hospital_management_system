package controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.IHospitalDao;
import dao.IHospitalDaoImpl;
import model.Admin;
import model.Patient;
import model.PatientDiagnosisDetails;
import model.Physician;

@Controller
public class AdminController {
  IHospitalDao dao = new IHospitalDaoImpl();

  @RequestMapping("login")
  public ModelAndView AdminLoginValid(@ModelAttribute("a") Admin admin) {
    boolean isValid = dao.adminAuthentication(admin);
    if (isValid) {
      return new ModelAndView("admin", "a", "Login Successfully");
    } else {
      return new ModelAndView("index", "a", "Invalid Credentials");
    }

  }

  @RequestMapping("enrollPatient")
  public ModelAndView register() {
    return new ModelAndView("enrollPatient", "p", new Patient());/// enrollpateint.jsp

  }

  @RequestMapping("Register")
  public ModelAndView add1(@ModelAttribute("p") Patient p) {
    dao.register(p);
    return new ModelAndView("display", "p", p);

  }

  @RequestMapping("addPhysician")
  public ModelAndView addPhysician() {
    return new ModelAndView("addPhysician", "p", new Physician());
  }

  @RequestMapping("adding")
  public ModelAndView add(@ModelAttribute("p") Physician p) {
    dao.addPhysician(p);
    return new ModelAndView("physicianView", "p", p);

  }

 

  @RequestMapping("patientDiagnosisDetails")
  public ModelAndView save() {
    return new ModelAndView("patientDiagnosisDetails", "p",
        new PatientDiagnosisDetails());

  }

  @RequestMapping("addDD")
  public ModelAndView add(@ModelAttribute("p") PatientDiagnosisDetails pd) {
    dao.save(pd);
    return new ModelAndView("diagnosisView", "p", pd);

  }

  @RequestMapping("viewHistory")
  public ModelAndView viewPatientHistory() {
    List<Patient> list = dao.viewPatientHistory();
    return new ModelAndView("viewHistory", "p", list);

  }
  @RequestMapping("searchPhysician")
  public ModelAndView searchPhysician(Physician p) {
    List<Physician> list=dao.searchPhysician(p);
    return new ModelAndView("searchPhysician","p",list);
    
  }
  @RequestMapping("search")
  public ModelAndView searchPhysician1(Physician p) {
    List<Physician> list=dao.searchPhysician(p);
    return new ModelAndView("search","p",list);
    
  }
  

}
