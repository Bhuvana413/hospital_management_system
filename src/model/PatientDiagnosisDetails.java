package model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="patientId")
public class PatientDiagnosisDetails extends Patient {
 
  private String diagnosisId;
  private String symptoms;
  private String diagnosisProvided;
  private String administeredBy;
  private String dateOfDiagnosis;
  private String followUpRequired;
  private String dateOfFollowUp;
  private Integer billAmount;
  private long cardNumber;
  private String modeOfAmount;
  
public PatientDiagnosisDetails() {
  // TODO Auto-generated constructor stub
}


public PatientDiagnosisDetails(Integer patientId, String diagnosisId,
    String symptoms, String diagnosisProvided, String administeredBy,
    String dateOfDiagnosis, String followUpRequired, String dateOfFollowUp,
    Integer billAmount, long cardNumber, String modeOfAmount) {
  super(patientId);
  this.diagnosisId = diagnosisId;
  this.symptoms = symptoms;
  this.diagnosisProvided = diagnosisProvided;
  this.administeredBy = administeredBy;
  this.dateOfDiagnosis = dateOfDiagnosis;
  this.followUpRequired = followUpRequired;
  this.dateOfFollowUp = dateOfFollowUp;
  this.billAmount = billAmount;
  this.cardNumber = cardNumber;
  this.modeOfAmount = modeOfAmount;
}


public String getDiagnosisId() {
  return diagnosisId;
}

public void setDiagnosisId(String diagnosisId) {
  this.diagnosisId = diagnosisId;
}

public String getSymptoms() {
  return symptoms;
}

public void setSymptoms(String symptoms) {
  this.symptoms = symptoms;
}

public String getDiagnosisProvided() {
  return diagnosisProvided;
}

public void setDiagnosisProvided(String diagnosisProvided) {
  this.diagnosisProvided = diagnosisProvided;
}

public String getAdministeredBy() {
  return administeredBy;
}

public void setAdministeredBy(String administeredBy) {
  this.administeredBy = administeredBy;
}

public String getDateOfDiagnosis() {
  return dateOfDiagnosis;
}

public void setDateOfDiagnosis(String dateOfDiagnosis) {
  this.dateOfDiagnosis = dateOfDiagnosis;
}

public String getFollowUpRequired() {
  return followUpRequired;
}

public void setFollowUpRequired(String followUpRequired) {
  this.followUpRequired = followUpRequired;
}

public String getDateOfFollowUp() {
  return dateOfFollowUp;
}

public void setDateOfFollowUp(String dateOfFollowUp) {
  this.dateOfFollowUp = dateOfFollowUp;
}

public Integer getBillAmount() {
  return billAmount;
}

public void setBillAmount(Integer billAmount) {
  this.billAmount = billAmount;
}

public long getCardNumber() {
  return cardNumber;
}

public void setCardNumber(long cardNumber) {
  this.cardNumber = cardNumber;
}

public String getModeOfAmount() {
  return modeOfAmount;
}

public void setModeOfAmount(String modeOfAmount) {
  this.modeOfAmount = modeOfAmount;
}
 
 
}
