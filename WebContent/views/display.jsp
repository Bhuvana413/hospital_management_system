<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success Page</title>
</head>
<body>
	<h1 align="center" style="color: #ff4da6;">Patient Details Preview</h1>
	<hr />
	<a href="index.jsp">Home</a><br/>
	<br />
	<br />
	<table border="1"
				style="with: 50%; margin-left: auto; margin-right: auto;">
	<tr style="color:#003366;">
	<td>Patient ID</td><td>${p.getPatientId()}</td>
	<tr/><tr style="color:#003366;">
	<td>First Name</td><td>${p.getFirstName()}</td>
	<tr/><tr style="color:#003366;">
	<td>Last Name</td><td>${p.getLastName()}</td>
	<tr/><tr style="color:#003366;">
	<td>Password</td><td>${p.getPassword()}</td>
	<tr/><tr style="color:#003366;">
	<td>Date of Birth</td><td>${p.getDateOfBirth()}</td>
	<tr/><tr style="color:#003366;">
	<td>Email Address</td><td>${p.getEmail()}</td>
	<tr/><tr style="color:#003366;">
	<td>Contact Number</td><td>${p.getContactNumber()}</td>
	<tr/><tr style="color:#003366;">
	<td>State</td><td>${p.getState()}</td>
	<tr/><tr style="color:#003366;">
	<td>Insurance Plan</td><td>${p.getInsurancePlan()}</td>
	</tr>
	
	</table>
	
	<h4 align="center" style="color: #ff4da6;">Registered Successfully</h4>
</body>
</html>