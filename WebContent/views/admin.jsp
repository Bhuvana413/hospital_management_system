<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin page</title>
<style>
container {
	margin-top: 120px;
	max-width: 600px;
	height: 320px;
	border: 1px solid #9C9C9C;
	background-color: #EAEAEA;
}

body {
	background-image:
		url("https://images.unsplash.com/photo-1611694449252-02453c27856a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80");
		
}
</style>
</head>
<body>
	<h1 align="center" style="color: #ccff66;">welcome to the Admin
		Page</h1>
	<hr />
	

	<form id="select" action="next" method="post">
		<br /> <br />
		<button style="background-color:#ffffcc;width: 16.6%" >
			<a href="addPhysician">Add Physician</a>
		</button>
		<br />
		<button style="background-color:#ffffcc;width: 16.6%">
			<a href="enrollPatient">Enroll Patient</a>
		</button>
		<br />
		<button style="background-color:#ffffcc;width: 16.6%">
			<a href="patientDiagnosisDetails">PatientDiagnosisDetails</a>
		</button>
		<br />
		<button style="background-color:#ffffcc;width: 16.6%">
			<a href="searchPhysician">Search Physician</a>
		</button>
		<br />
		<button style="background-color:#ffffcc;width: 16.6%">
			<a href="viewHistory">View History</a>
		</button>
		<br />
		<button style="background-color:#ffffcc;width: 16.6%">
			<a href="index.jsp">Home</a>
		</button>
	</form>
</body>
</html>