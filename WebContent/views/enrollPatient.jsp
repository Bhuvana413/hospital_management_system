<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
<style>
container {
	margin-top: 120px;
	max-width: 600px;
	height: 320px;
	border: 1px solid #9C9C9C;
	background-color: #EAEAEA;
}

body {
	background-image:
		url("https://images.unsplash.com/photo-1536064479547-7ee40b74b807?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80");
}
</style>
</head>
<body>
	<a href="index.jsp">Home</a>
	<br />
	<a href="index">Admin Page</a>
	<h1 align="center" style="color: #003366;">Enroll Patient Details</h1>

	<hr />
	<!-- <div style="height:20%;width:20%;top:20%;bottom:20%">
	<img src="https://images.unsplash.com/photo-1505751172876-fa1923c5c528?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1050&q=80" alt="hospital"/>
	</div> -->
	<s:form action="Register" method="post" modelAttribute="p">

		<table border="0"
				style="with: 50%; margin-left: auto; margin-right: auto;">
			<tr>
				<td style="color:#003366;">Patient ID :</td>
				<td><s:input path="patientId" autocomplete="off" /></td>
			</tr>
			<tr>
			<tr>
				<td style="color:#003366;">First Name :</td>
				<td><s:input path="firstName" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">Last Name :</td>
				<td><s:input path="lastName" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">Password:</td>
				<td><s:input path="password" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">Date of Birth :</td>
				<td><s:input path="dateOfBirth" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">Email Address :</td>
				<td><s:input path="email" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">Contact Number :</td>
				<td><s:input path="contactNumber" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">State :</td>
				<td><s:input path="state" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003366;">Insurance Plan :</td>
				<td><s:input path="insurancePlan" autocomplete="off" /></td>
			</tr>
			<tr>
				<td></td>
				<td><s:button>Register</s:button></td>
			</tr>

		</table>
	</s:form>
</body>
</html>