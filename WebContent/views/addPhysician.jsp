<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Physiain</title>
<style>
container {
	margin-top: 120px;
	max-width: 600px;
	height: 320px;
	border: 1px solid #9C9C9C;
	background-color: #EAEAEA;
}

  body {
	background-image:url("https://images.unsplash.com/photo-1584432810601-6c7f27d2362b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80");
	} 
</style>
</head>
<body>
	<a href="index.jsp">Home</a><br/>
	<h1 align="left" style="color: #003300;">Add Physician Here</h1>
	<hr />
	<s:form action="adding" method="post" modelAttribute="p">
		<table 
				style="with: 50%; margin-right: auto;">
			<tr>
				<td style="color:#003300;">Physician ID:</td>
				<td><s:input path="physicianId" autocomplete="off"
						readonly="readonly" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">Physician FirstName:</td>
				<td><s:input path="physicianFirstName" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">Physician LastName:</td>
				<td><s:input path="physicianLastName" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">Department:</td>
				<td><s:input path="department" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">Educational Qualification:</td>
				<td><s:input path="educationalQualification" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">Experience:</td>
				<td><s:input path="yearsOfExperience" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">State:</td>
				<td><s:input path="state" autocomplete="off" /></td>
			</tr>
			<tr>
				<td style="color:#003300;">Insurance Plan:</td>
				<td><s:input path="insurancePlan" autocomplete="off"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Register" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="reset" value="Reset" /></td>
			</tr>
		</table>
	</s:form>
</body>
</html>