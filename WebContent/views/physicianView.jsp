<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
container {
	margin-top: 120px;
	max-width: 600px;
	height: 320px;
	border: 1px solid #9C9C9C;
	background-color: #EAEAEA;
}

  body {
	background-image:url("https://images.unsplash.com/photo-1611694449252-02453c27856a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80");
	} 
</style>
</head>
<body>
<h1>Physician Preview</h1>
	<hr />
	<a href="index.jsp">Home</a>
	<br />
	<br />
	<table border="1">
		<tr>
			<td>Physician ID</td>
			<td>${p.getPhysicianId()}</td>
		<tr />
		<tr>
			<td>Physician FirstName</td>
			<td>${p.getPhysicianFirstName()}</td>
		<tr />
		<tr>
			<td>Physician LastName</td>
			<td>${p.getPhysicianLastName()}</td>
		<tr />
		<tr>
			<td>Department</td>
			<td>${p.getDepartment()}</td>
		<tr />
		<tr>
			<td>Educational Qualification</td>
			<td>${p.getEducationalQualification()}</td>
		<tr />
		<tr>
			<td>Experience</td>
			<td>${p.getYearsOfExperience()}</td>
		<tr />
		<tr>
			<td>State</td>
			<td>${p.getState()}</td>
		<tr />
		<tr>
			<td>Insurance Plan</td>
			<td>${p.getInsurancePlan()}</td>
		<tr />
		

	</table>
	
	<h4>Saved Successfully</h4>
</body>
</html>