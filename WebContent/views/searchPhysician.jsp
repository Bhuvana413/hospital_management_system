<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search Physician</title>
</head>
<body>
	<h1 align="center" style="color: #ff4da6;">Physician Search Page</h1>
	<hr />
	<form id="find" action="search" method="post">
		<table border="1"
				style="with: 50%; margin-left: auto; margin-right: auto;">
			<tr style="color:#993333;">
				<td>State:</td>
				<td><input type="text" name="state" autocomplete="off"
					class="checkbox" /></td>
			</tr>
			<tr style="color:#993333;">
				<td>Insurance Plan:</td>
				<td><input type="text" name="insurancePlan" autocomplete="off"
					class="checkbox" /></td>
			</tr>
			<tr style="color:#993333;">
				<td>Department:</td>
				<td><input type="text" name="department" autocomplete="off"
					class="checkbox" /></td>
			</tr>
			<tr >
				<td></td>
				<td><input type="submit" value="Search" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="reset" value="Reset" /></td>
			</tr>
		</table>
	</form>
</body>
</html>