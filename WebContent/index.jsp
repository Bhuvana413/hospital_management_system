<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style>
container {
	margin-top: 120px;
	max-width: 600px;
	height: 320px;
	border: 1px solid #9C9C9C;
	background-color: #EAEAEA;
}

  body {
	background-image:url("https://images.unsplash.com/photo-1597807037496-c56a1d8bc29a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80");
	} 
</style>
</head>
<body>


	<h1 align="center" style="color: #ff4da6;">Welcome to HCL
		technologies</h1>
	<div class="container">
		<hr />
		<p align="center">
			<font color="blue">Please Enter UserName and Password </font>
		</p>
		<form id="signin" action="login" method="post">
			${error}<br />

			<table border="0"
				style="with: 50%; margin-left: auto; margin-right: auto;">
				<tr align="center" valign="top">
					<td>Username:</td>
					<td><input type="text" name="userName" class="inputbox"
						autocomplete="off" autofocus="on" placeholder="Username" /></td>
				</tr>

				<tr align="center" valign="top">
					<td>Password:</td>
					<td><input type="password" name="password" class="inputbox"
						autocomplete="off" autofocus="on" placeholder="Password" /></td>
				</tr>
				<tr align="center" valign="top">
					<td></td>
					<td><input type="submit" value="login" class="submitButton" /></td>
				</tr>
			</table>

		</form>
	</div>
</body>
</html>